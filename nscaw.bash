#!bash
#
# Copyright (C) 2014--2018, 2021 Tom Ryder <tom@sanctum.geek.nz>
#
# This file is part of nscaw.
#
# nscaw is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# nscaw is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# nscaw.  If not, see <https://www.gnu.org/licenses/>.
#

#
# Command wrapper that sends an appropriate passive check to an NSCA server
# using send_nsca, depending on the command's outcome.
#
# Make sure send_nsca is in $PATH and that NSCAW_SERVER is set.
#
#   $ nscaw IMPORTANT_JOB -- important-job -o options args ...
#

# Name ourself
self=nscaw

# If there's a defaults file with environment variables for us, source it
if [[ -r /etc/default/$self ]] ; then
	source /etc/default/"$self"
fi

# Define a function to explain how to use this script
usage() {
	printf 'Usage: %s SERVICE -- COMMAND...\n' \
		"$self"
}

# Respond to requests for help with usage (exit success)
case $1 in
	-h|--help)
		usage
		exit 0
		;;
esac

# Check that at least three arguments are present and in the correct form
if (( $# < 3 )) || [[ $2 != -- ]] ; then
	usage >&2
	exit 1
fi

# Pull the service from the first argument and shift the first two arguments
# off the arguments array; the rest of the array should be the command to run
service=$1
shift 2

# Figure out our hostname; most of the time `hostname -s` will be fine, but
# it can be overridden with the value of NSCAW_HOSTNAME if defined
hostname=${NSCAW_HOSTNAME:-$(hostname -s)}

# Figure out our username
username=$(whoami)

# Attempt to run command within a time wrapper
# <http://mywiki.wooledge.org/BashFAQ/032>
exec 3>&1 4>&2
TIMEFORMAT=$'real %3lR, user %3lU, sys %3lS\n' \
	time=$( { time "$@" 1>&3 2>&4 ; } 2>&1) ; ret=$?
exec 3>&- 4>&-

# Decide return code and message based on command exit value
case $ret in
	0)
		code=0 # OK
		message=$(printf '%s: `%s` succeeded: %s' \
			"$self" "$*" "$time")
		;;
	*)
		code=2 # CRITICAL
		message=$(printf '%s: `%s` failed (exit %d), USER %s, PATH=%s: %s' \
			"$self" "$*" "$ret" "$username" "$PATH" "$time")
		;;
esac

# Format the passive check and pipe it into send_nsca; note that we ignore the
# stdout of send_nsca as it's just a diagnostic message
printf '%s\t%s\t%u\t%s\n' \
	"$hostname" "$service" "$code" "$message" |
		send_nsca -H "${NSCAW_SERVER:-nsca}" \
			-c "${NSCAW_CONFIG:-/etc/send_nsca.cfg}" >/dev/null
