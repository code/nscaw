nscaw
=====

Command wrapper that sends an appropriate passive check to an NSCA server using
`send_nsca`, depending on the command's outcome.

Make sure `send_nsca` is in `$PATH`. You'll almost certainly want to set
`NSCA_SERVER` in `/etc/default/nscaw` as well, but it makes an effort in
defaulting to `nsca`.

    $ nscaw IMPORTANT_JOB -- important-job -o options args ...

License
-------

Copyright (C) 2014--2018, 2021 Tom Ryder <tom@sanctum.geek.nz>

Distributed under GNU General Public License version 3 or any later version.
Please see `COPYING`.
